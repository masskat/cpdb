# README #

Simple Perl module for reading and modifying Check Point objects and rules databases. It reads directly *'objects_5_0.C'* and *'rulebases_5_0.fws'* files. Produces output in the dbedit or XLSX (Excel) format.

### Features ###

* Supports Check Point releases from **R65** up to **R77.30**.
* Reads object databases (*'objects_5_0.C'*) and rule bases files (*'rulebases_5_0.fws'*).
* Exports acquired configuration into the **dbedit** or **Excel** (XLSX) formats.
* Finds objects within other objects (e.g. groups) and rules.
* Compares objects and their parameters.
* Converts automatic NAT rules into manual rules.

### Limitations ###

Module supports following types of objects:

* **Network** (*network* class),
* **Host** (*host_plain* class),
* **Network object group** (*network_object_group* class),
* **Group with exclusions** (*group_with_exception* class),
* **IP address ranges** (*address_range* class),
* **TCP service** (*tcp_service* class),
* **UDP service** (*udp_service* class),
* **ICMP service** (*icmp_service* class),
* **Generic IP service** (*other_service* class),
* **RPC service** (*dcerpc_service* class),
* **Service group** (*service_group* class).

At the moment the module supports only **Security** and **NAT** rule bases. Most of the not supported object types can be read and written into rules (e.g. VPN communities), however they need to be created manually first. Currently following items and features are not supported at all:

* IPv6.
* Rule bases in Legacy mode.
* Legacy user groups in source fields. 
* 'Time' column. In outputs it will be always set to 'Any'.
* Legacy actions (user/client/session authentication).

### Dependencies ###

The module can be run on **Perl v5.8.2** or later. There are no restrictions for the operating system, however please remember that the dbedit output file must have UNIX line endings.

Following additional modules are required:

* **Excel::Writer::XLSX** (version 0.88 or higher)

### Usage ###

Module is available as a single file. To use it, simply save it on a computer and declare it in your script:

    #!/usr/bin/perl
	
    require 'cpdb.pm';

### Examples ###

Can be found in the **examples** folder:

* **rules-export.pl** - Script which allows exporting firewall and NAT rules directly from the *'objects_5_0.C'* and *'rulebases_5_0.fws'* files into the dbedit and XLSX (Excel) format. Besides specific rules, script exports also all used objects within those rules. This allows to easily dbedit configuration on a different Security Management Server.

### License ###

This project is released under the MIT license. See the LICENSE.txt file for details.
    