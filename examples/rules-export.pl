#!/usr/bin/perl

use warnings;
use Getopt::Long;
use Excel::Writer::XLSX;
require 'cpdb.pm';	


######################################################################################
## README
######################################################################################

#
# Script allows exporting firewall and NAT rules directly from the 'objects_5_0.C'
# and 'rulebases_5_0.fws' files into the dbedit and XLSX (Excel) format.
# Besides specific rules, script exports also all used objects within those rules.
# This allows to easily dbedit configuration on a different Security Management Server.
#
# Requirements:
# 
#   Script requires a copy of the 'cpdb.pm' module in the working directory.
#
# Usage:
#
#   perl rules-export.pl [parameters]
#
# Parameters:
#
#   --inobjects  (mandatory) - Source (input) objects file (objects_5_0.C).
#
#   --inrules    (manfatory) - Source (input) rule bases file (rulebases_5_0.fws).
#
#   --inpolicy   (optional)  - Source (input) Policy Package. Default is 'Standard'.
#
#   --inrange    (optional)  - Source (input) rules range
#                              ('x-x' - specific range,  '-x', 'x-' - all rules from/to specific number). If not specified, all rules are exported.
#                              If the specified range contains automatic NAT rules, they will be omitted.
#                              Use '--intype autonat' to export automatic NAT rules.
#
#   --intype     (optional)  - Source (input) rules type
#                              ('fw' - firewall rules, 'nat' - NAT rules, except automatic rules, 'autonat' - all automatic rules). Default is 'fw'.
#
#   --outobjects (optional)  - Destination (output) objects file (objects_5_0.C) to be compared with the input objects file.
#                              If not specified, script assumes that all objects must be created.
#
#   --outrules   (optional)  - Destination (output) rule bases file (rulebases_5_0.fws).
#                              Used to calculate the position of the exported rules (at the end of the specified destination Policy Package).
#                              If not specified, it is assumed that the destination Policy Package is empty.
#
#   --outpolicy  (optional)  - Destination (output) Policy Package. Default is 'Standard'.
#
#   --fileprefix (optional)  - Prefix for output .txt and .xlsx files. Default is 'rules-export'.
#
# Example:
#
#   perl rules-export.pl --inobjects objects_5_0-in.C --inrules rulebases_5_0-in.fws --inrange 100-254 --outobjects objects_5_0-out.C --outrules rulebases_5_0-out.fws --outpolicy Test --fileprefix testexport
#
#   Export firewall rules (by default), from no 100 to 254, from the 'Standard' Policy Package (default) defined in the 'rulebases_5_0-in.fws' file,
#   together with all associated objects defined in the 'objects_5_0-in.C' file. Compare them with the database defined in 'objects_5_0-out.C'
#   and export only unique objects. Rules will be added at the end of the 'Test' Policy Package defined in the 'rulebases_5_0-out.fws' file.
#   Script will create two files: dbedit - 'testexport.txt' and Excel - 'testexport.xlsx', both having the 'testexport' prefix.
#


######################################################################################
## General script variables
######################################################################################


# Object and rules databases.
my $ref_incpdb	= {};		# Source (input) database.
my $ref_outcpdb	= {};		# Destination (output) database.

# Arrays with rules.
my @rules_exp	= ();		# Contains exported rules.
my @autonat		= ();		# Contains a list of automatic rules.

# Hashes with objects to be created or replaced.
my %createdobj	= ();		# Hash containing objects to be created.
my %replaceobj	= ();		# Hash containing information about object replacements.

# First and last rules to be migrated.
my $rule_start	= 0;
my $rule_end	= 0;

# Rules counters.
my $ruleno		= 1; 		# Rule number as it appears in the SmartDashboard.
my $ruleid		= 0;		# Real rule id used by dbedit.

# Script cmd parameters.
my $inobjects	= '';				# Source (input) objects file (objects_5_0.C).
my $inrules		= '';				# Source (input) rule bases file (rulebases_5_0.fws).
my $inpolicy	= 'Standard';		# Source (input) Policy Package. Default is 'Standard'.
my $inrange		= '';				# Source (input) rules range (x-x, -x, x- formats available).
my $intype		= 'fw';				# Source (input) rules type (fw, nat, autonat). Default is 'fw'.
my $outobjects	= '';				# Destination (output) objects file (objects_5_0.C).
my $outrules	= '';				# Destination (output) rule bases file (rulebases_5_0.fws).
my $outpolicy	= 'Standard';		# Destination (output) Policy Package. Default is 'Standard'.
my $fileprefix	= 'rules-export';	# Prefix for output .txt and .xlsx files. Default is 'rules-export'.

# Rule types.
my %rule_type	= (
	'fw'	=> 'rule',
	'nat'	=> 'rule_adtr'
);

# Hash containing preferred object names.
my %preferobj	= (
	'http'			=> 1,
	'https'			=> 1,
	'ssh'			=> 1,
	'ftp'			=> 1,
	'snmp'			=> 1,
	'dhcp-relay'	=> 1,
	'icmp-proto'	=> 1,
	'traceroute'	=> 1
);


######################################################################################
## Getting values for the general script variables
######################################################################################


# Turn off buffering for STDOUT.
$| = 1;

# Get command line options.
GetOptions (
	'inobjects=s'	=> \$inobjects,
	'inrules=s'		=> \$inrules,
	'inpolicy=s'	=> \$inpolicy,
	'inrange=s'		=> \$inrange,
	'intype=s'		=> \$intype,
	'outobjects=s'	=> \$outobjects,
	'outrules=s'	=> \$outrules,
	'outpolicy=s'	=> \$outpolicy,	
	'fileprefix=s'	=> \$fileprefix

);

# Calculate the first and the last rule number ($rule_end = 0 means infinity).
$rule_start	= $1 if $inrange =~ /([0-9]+)\-.*/;
$rule_end	= $1 if $inrange =~ /.*\-([0-9]+)/;

# Get input and output objects and rules.
print "Reading Check Point configuration...";
if ($inobjects && $inrules) {		# Input objects and rules files must be defined.
	$ref_incpdb = cpdb::read(
		{
			'objects'	=> $inobjects,
			'rulebases'	=> $inrules,
			'policypkg'	=> $inpolicy
		}
	);	
} else {
	die "Parameters --inobjects and --inrules are mandatory.\n"
}
if ($outobjects || $outrules) {	# Output objects and rules files are optional.
	my %params;	
	$params{'objects'}		= $outobjects	if $outobjects;
	$params{'rulebases'}	= $outrules		if $outrules;
	$params{'policypkg'}	= $outpolicy	if $outrules;	
	$ref_outcpdb = cpdb::read(\%params);
}
print " Done.\n";



######################################################################################
## Calculating needed rules and objects
######################################################################################


# Look for objects with automatic NAT.
# Write them in the %createdobj hash if the 'autonat' option is used in the cmd.
# Convert objects to manual NAT rules (only for presenting them in the excel file or for calculating the amount of automatic NAT rules).
print "Looking for automatic NAT configuration...";
foreach my $obj (keys %{$ref_incpdb->{'network_objects'}}) {
	if (exists $ref_incpdb->{'network_objects'}{$obj}{'add_adtr_rule'} && $ref_incpdb->{'network_objects'}{$obj}{'add_adtr_rule'} eq 'true') {
		$createdobj{'network_objects'}{$obj} = $ref_incpdb->{'network_objects'}{$obj} if $intype eq 'autonat';
		my $ref_auto = cpdb::auto2manual($obj, $ref_incpdb->{'network_objects'}{$obj});	
		push @autonat, @{$ref_auto->{'rules'}};
	}
}
print " Done.\n";


# Option 'intype' is not set to 'autonat'.
# Extract rules and objects that need to be exported. Find replacements if needed.
if ($intype ne 'autonat') {

	print "Exporting rules and corresponding objects...";

	# Extract rules that need to be exported.
	my $blockno = 0;	# Current block number in NAT rules.
	foreach my $ref_rule (@{$ref_incpdb->{'rulebases'}{$inpolicy}{$rule_type{$intype}}}) {

		# Move the rule number counter when a new NAT block is found ('rule_block_number' changes from 0 to 1).
		if ($intype eq 'nat') {
			$ruleno += scalar @autonat if $blockno != $ref_rule->{'rule_block_number'};
			$blockno = $ref_rule->{'rule_block_number'};
		}
	
		# Export a rule that is within the specified range.
		push @rules_exp, $ref_rule if $ruleno >= $rule_start && ($rule_end == 0 || $ruleno <= $rule_end);
	
		# Increment rule number counter.
		$ruleno++ if $ref_rule->{'class'} =~ /security_rule|address_translation_rule/;
	
	}

	# Get all used objects within exported rules.
	my $ref_objused = cpdb::objused(\@rules_exp, {'recursive' => $ref_incpdb});

	# Populate the %createdobj and %replaceobj hases and check which objects must be migrated manually.
	foreach my $type (keys %$ref_objused) {
		foreach my $obj (keys %{$ref_objused->{$type}}) {
			if (exists $ref_incpdb->{$type}) {
				if (exists $ref_incpdb->{$type}{$obj}) {
					if ($type =~ /network_objects|services/) {
						
						# Add to the hash with objects to be created.
						$createdobj{$type}{$obj} = $ref_incpdb->{$type}{$obj};
						
						# Look for preplacements.
						if ($outobjects) {	

							# Get a list of objects matching currently processed object.
							my $ref_match = cpdb::compare($obj, $ref_incpdb->{$type}{$obj}, $ref_outcpdb);
							
							# Handle replacements.
							if (exists $ref_match->{'match'}) {
								my $preferred;		# Contains information about a preferred object.
								foreach (@{$ref_match->{'match'}}) { $preferred = $_ if exists $preferobj{$_} }
								$replaceobj{$type}{$obj} = (defined $preferred) ? $preferred : $ref_match->{'match'}[0];		# Write preferred replacement. If doesn't exist, write the first replacing object.
							}
							
							# Handle overlaping objects.
							if (exists $ref_match->{'overlap'}) {
								print "\n WARNING Object '$obj' ($type) conflicts with '$ref_match->{'overlap'}' from the destination database.";
								delete $createdobj{$type}{$obj};
							}
							
						}
				
					}
				} else {
					print "\n WARNING Object '$obj' ($type) must be migrated manually.";
				}
			} else {
				print "\n WARNING Object '$obj' ($type) must be migrated manually.";
			}
		}
	}
	
	print " Done.\n";

} 



######################################################################################
## Printing output
######################################################################################


print "Generating dbedit configuration and the excel report...";


# Open a filehandle to write dbedit commands..
open DBEDIT, '>', "$fileprefix.txt" or die "Can't write to $fileprefix.txt: $!";


# Generic Excel variables.
my $column	= 0;	# Column counter.
my $row		= 1;	# Row counter.

# Create a new Excel file.
my $wbook = Excel::Writer::XLSX->new("$fileprefix.xlsx");

# Add work sheets.
my $wsheet_obj	= $wbook->add_worksheet('Objects');
my $wsheet_rul 	= ($intype eq 'fw') ? $wbook->add_worksheet('Firewall rules') : $wbook->add_worksheet('NAT rules');

# Set columns width in particular excel sheets.
$wsheet_obj->set_column(0, 1, 20);
$wsheet_obj->set_column(2, 2, 25);
$wsheet_obj->set_column(3, 3, 40);
$wsheet_obj->set_column(5, 5, 30);
if ($intype eq 'fw') {
	$wsheet_rul->set_column(2, 2, 20);
	$wsheet_rul->set_column(3, 6, 40);
	$wsheet_rul->set_column(9, 9, 10);
	$wsheet_rul->set_column(10, 10, 40);
} else {
	$wsheet_rul->set_column(2, 7, 40);
	$wsheet_rul->set_column(8, 8, 10);
	$wsheet_rul->set_column(9, 9, 40);
}

# Define header formatting.
my $fhead = $wbook->add_format(
	'bold'		=> 1,
	'border'	=> 1,
	'color'		=> 'white',
	'bg_color'	=> 'green'
);

# Write headers.
foreach ('Table', 'Object Name', 'Class', 'Properties', 'Color', 'Comment') {
	$wsheet_obj->write(0, $column, $_, $fhead);
	$column++;
}
$column = 0;
if ($intype eq 'fw') {
	foreach ('Disabled', 'No.', 'Name', 'Source', 'Destination', 'VPN', 'Service', 'Action', 'Track', 'Install', 'Comment') {
		$wsheet_rul->write(0, $column, $_, $fhead);
		$column++;
	}
} else {
	foreach ('Disabled', 'No.', 'Source', 'Destination', 'Service', 'Source Translated', 'Destination Translated', 'Service Translated', 'Install', 'Comment') {
		$wsheet_rul->write(0, $column, $_, $fhead);
		$column++;
	}
}


# Create network and service objects (dbedit, excel).

print DBEDIT "#\n##################################################\n";
print DBEDIT "## Creating new objects                         ##\n";
print DBEDIT "##################################################\n";

foreach my $type ('network_objects', 'services') {

	# Contains commands to add group members. They will be printed at the end, to make sure that all objects are added before adding them to groups.
	my %grpmembers;	

	# Print objects (dbedit, excel). Save group members in a separate variable. They will be printed at the end. Replace group members when necessary. 
	foreach my $obj (sort keys %{$createdobj{$type}}) {
	
		# Check object syntax.
		my $ref_error = cpdb::check($createdobj{$type}{$obj});
		print "\nWARNING Check parameters in object '$obj': @$ref_error..." if @$ref_error;
	
		# Write an object to an excel file.
		cpdb::xlsx($row, $wsheet_obj, $wbook, $obj, $createdobj{$type}{$obj}, {'cmp' => \%replaceobj, 'replace' => 'show_both'});
		$row++;
		
		# Write an object to a file (dbedit format).
		my $ref_dbedit = cpdb::dbedit($obj, $createdobj{$type}{$obj}, {'replace' => 'omit_old', 'cmp' => \%replaceobj});
		print DBEDIT "#\n# Creating object '$obj'\n#\n";
		foreach (@$ref_dbedit) {
			if ($_ =~ /addelement/) {
				push @{$grpmembers{$obj}}, $_;
			} else {
				print DBEDIT "$_\n";
			}
		}

	}
	
	# Print group members in a file (dbedit format).
	if (%grpmembers) {
		print DBEDIT "#\n# Adding $type group members\n#\n"; 
		foreach my $obj (sort keys %grpmembers) {
			foreach (@{$grpmembers{$obj}}) { print DBEDIT "$_\n" }
			print DBEDIT "update $type $obj\n";
		}
	}
	
}


# Create firewall and NAT rules (dbedit, excel).

$ruleno	= 1;
$row	= 1;

if ($intype ne 'autonat') {

	print DBEDIT "#\n##################################################\n";
	print DBEDIT "## Creating firewall and NAT rules              ##\n";
	print DBEDIT "##################################################\n";

	$ruleid	= scalar @{$ref_outcpdb->{'rulebases'}{$outpolicy}{$rule_type{$intype}}} if $outrules && exists $ref_outcpdb->{'rulebases'}{$outpolicy}{$rule_type{$intype}};

	# Print initial section in the dbedit format.
	my $ref_section = cpdb::dbedit_sectitle($rule_type{$intype}, $ruleid, "### Imported rules ###", {'policypkg' => $outpolicy});
	print DBEDIT "#\n# Creating $rule_type{$intype} $ruleid\n#\n";
	foreach (@$ref_section) { print DBEDIT "$_\n" }
	$ruleid++;

	# Print exported rules. 
	foreach my $ref_rule (@rules_exp) {
	
		# Check rule syntax.
		my $ref_error = cpdb::check($ref_rule);
		print "\n WARNING Check parameters in $rule_type{$intype} $ruleno: @$ref_error." if @$ref_error;

		# Write a rule to an excel file.
		cpdb::xlsx($row, $wsheet_rul, $wbook, $ruleno, $ref_rule, {'cmp' => \%replaceobj, 'replace' => 'show_both'});
		$row++;
	
		# Write a rule to a file (dbedit format).
		my $ref_dbedit = cpdb::dbedit($ruleid, $ref_rule, {'policypkg' => $outpolicy, 'replace' => 'omit_old', 'cmp' => \%replaceobj});
		print DBEDIT "#\n# Creating $rule_type{$intype} $ruleid\n#\n";
		pop @$ref_dbedit if $ruleid % 10;	# Update rulebase every 10 rules.
		foreach (@$ref_dbedit) { print DBEDIT "$_\n" }
		
		# Increment rule counters.
		$ruleid++;
		$ruleno++ if not($ref_rule->{'class'} =~ /header_rule/);

	}
	
	# Add 'update' command at the end of the firewall and NAT rulebase.
	print DBEDIT "update fw_policies ##$outpolicy\n";
	
} else {
	# Write autonat rules to an excel file.
	foreach my $ref_rule (@autonat) {
		cpdb::xlsx($row, $wsheet_rul, $wbook, $ruleno, $ref_rule);
		$row++;
		$ruleno++;
	}
}
	
# Close the filehandle for the dbedit file.
close DBEDIT;
print " Done.\n";
