#!/usr/bin/perl

use warnings;
use Excel::Writer::XLSX;

# cpdb package definition.
package cpdb;


#####################################################################################
# Common variables.
#####################################################################################


# Hash containing relations between classes and object/rule types.
my %types	= (
	'network'					=> 'network_objects',	
	'host_plain'				=> 'network_objects',
	'network_object_group'		=> 'network_objects',
	'group_with_exception'		=> 'network_objects',
	'address_range'				=> 'network_objects',
	'tcp_service'				=> 'services',
	'udp_service'				=> 'services',
	'icmp_service'				=> 'services',
	'other_service'				=> 'services',
	'dcerpc_service'			=> 'services',
	'service_group'				=> 'services',
	'security_rule'				=> 'rule',
	'security_header_rule'		=> 'rule',
	'address_translation_rule'	=> 'rule_adtr',
	'nat_header_rule'			=> 'rule_adtr'
);

# Stack used to point the current section of the Check Point config file.
my @stack	= ('MAIN');	

# Data structure defining information that has to be taken from the objects_5_0.C file.
my %objdef 	= (

	'MAIN'				=> {
		'sections'	=> {
			'globals'			=> qr/:globals \($/,
			'network_objects'	=> qr/:network_objects \($/,
			'services'			=> qr/:services \($/
		}
	},
	
	'globals'			=> {
		'sections'	=> {
			'object'			=> qr/: \(([a-zA-Z0-9_\-\.]+)/
		}
	},
		
	'network_objects' 	=> {
		'sections'	=> {
			'object'			=> qr/: \(([a-zA-Z0-9_\-\.]+)/
		}
	},
	
	'services' 			=> {
		'sections'	=> {
			'object'			=> qr/: \(([a-zA-Z0-9_\-\.]+)/
		}
	},
	
	'object'			=> {
		'sections'	=> {
			'admininfo'				=> qr/:(AdminInfo) \($/,
			'referenceobject'		=> qr/: \((ReferenceObject)/,
			'the_firewalling_obj'	=> qr/:(the_firewalling_obj) \(ReferenceObject/,
			'proto_type'			=> qr/:(proto_type) \(ReferenceObject/,
			'base'					=> qr/:(base) \(ReferenceObject/,
			'exception'				=> qr/:(exception) \(ReferenceObject/,
			'interfaces'			=> qr/:(interfaces) \($/		# Dummy definition, just to hide another 'netmask' parameter inside.
		},
		'data'		=> {
			'color'					=> qr/:color \((.*)\)/,
			'comments'				=> qr/:comments \((.*)\)/,
			'ipaddr'				=> qr/:ipaddr \(([0-9\.]+)\)/,
			'ipaddr_first'			=> qr/:ipaddr_first \(([0-9\.]+)\)/,
			'ipaddr_last'			=> qr/:ipaddr_last \(([0-9\.]+)\)/,
			'netmask'				=> qr/:netmask \(([0-9\.]+)\)/,
			'port'					=> qr/:port \(([0-9>"\-]+)\)/,
			'add_adtr_rule'			=> qr/:add_adtr_rule \((true|false)\)/,
			'netobj_adtr_method'	=> qr/:netobj_adtr_method \((adtr_hide|adtr_static)\)/,
			'valid_ipaddr'			=> qr/:valid_ipaddr \(([0-9\.]+)\)/,
			'valid_ipaddr6'			=> qr/:valid_ipaddr6 \((.*)\)/,
			'protocol'				=> qr/:protocol \(([0-9\-]+)\)/,
			'icmp_type'				=> qr/:icmp_type \(([0-9]+)\)/,
			'uuid'					=> qr/:uuid \(([a-zA-Z0-9\-]+)\)/,
			'timeout'				=> qr/:timeout \(([0-9]+)\)/
		}
	},
	
	'admininfo'			=> {
		'data'	=> {
			'class'		=> qr/:ClassName \((any_object|network|host_plain|network_object_group|group_with_exception|address_range|tcp_service|udp_service|icmp_service|other_service|dcerpc_service|service_group)\)/,
			'global'	=> qr/:global_level \((1)\)/
		}
	},
	
	'referenceobject'	=> {
		'data'	=> {
			'reference'	=> qr/:Name \((.*)\)/,
			'table'		=> qr/:Table \(([a-zA-Z0-9_]+)\)/
		}
	},

	'the_firewalling_obj'	=> {
		'data'	=> {
			'name'	=> qr/:Name \((.*)\)/,
			'table'	=> qr/:Table \(([a-zA-Z0-9_]+)\)/
		}
	},
	
	'proto_type'			=> {
		'data'	=> {
			'name'	=> qr/:Name \((.*)\)/,
			'table'	=> qr/:Table \(([a-zA-Z0-9_]+)\)/
		}
	},
	
	'base'		=> {
		'data'	=> {
			'name'	=> qr/:Name \((.*)\)/,
			'table'	=> qr/:Table \(([a-zA-Z0-9_]+)\)/
		}
	},
	
	'exception'	=> {
		'data'	=> {
			'name'	=> qr/:Name \((.*)\)/,
			'table'	=> qr/:Table \(([a-zA-Z0-9_]+)\)/
		}
	}
	
);

# Data structure defining information that has to be taken from the rulebases_5_0.fws file.
my %ruldef = (

	'MAIN'	=> {
		'sections'	=> {
			'rulebase'	=> qr/:rule-base \(\"##([a-zA-Z0-9_\-\.]+)\"/
		}
	},
	
	'rulebase'	=> {
		'sections'	=> {
			'ruleobj'	=> qr/:(rule|rule_adtr) \($/
		}
	},
	
	'ruleobj'	=> {
		'sections'	=> {
			'admininfo'					=> qr/:AdminInfo \($/,
			'src'						=> qr/:src \($/,
			'dst'						=> qr/:dst \($/,
			'services'					=> qr/:services \($/,
			'src_adtr'					=> qr/:src_adtr \($/,
			'dst_adtr'					=> qr/:dst_adtr \($/,
			'services_adtr'				=> qr/:services_adtr \($/,
			'src_adtr_translated'		=> qr/:src_adtr_translated \($/,
			'dst_adtr_translated'		=> qr/:dst_adtr_translated \($/,
			'services_adtr_translated'	=> qr/:services_adtr_translated \($/,
			'action'					=> qr/:action \($/,
			'track'						=> qr/:track \($/,
			'install'					=> qr/:install \($/,
			'through'					=> qr/:through \($/
			
		},
		'data'	=> {
			'name'				=> qr/:name \((.*)\)/,
			'comments'			=> qr/:comments \((.*)\)/,
			'disabled'			=> qr/:disabled \((true|false)\)/,
			'rule_block_number'	=> qr/:rule_block_number \((0|1)\)/,
			'header_text'		=> qr/:header_text \((.*)\)/,
			'state'				=> qr/:state \((collapsed|expanded)\)/
		}
	},
		
	'admininfo'	=> {
		'data'	=> {
			'class'		=> qr/:ClassName \((security_rule|address_translation_rule|security_header_rule|nat_header_rule)\)/,
			'global'	=> qr/:global_level \((1)\)/
		}
	},

	'src'	=> {
		'sections'	=> {
			'referenceobject'	=> qr/: \(ReferenceObject/
		},
		'data'		=> {
			'op'				=> qr/:op \((.*)\)/
		}
	},
	
	'dst'	=> {
		'sections'	=> {
			'referenceobject'	=> qr/: \(ReferenceObject/
		},
		'data'		=> {
			'op'				=> qr/:op \((.*)\)/
		}
	},

	'services'	=> {
		'sections'	=> {
			'referenceobject'	=> qr/: \(ReferenceObject/
		},
		'data'		=> {
			'op'				=> qr/:op \((.*)\)/
		}
	},
	
	'src_adtr'	=> {
		'sections'	=> {
			'referenceobject'	=> qr/: \(ReferenceObject/
		}
	},
	
	'dst_adtr'	=> {
		'sections'	=> {
			'referenceobject'	=> qr/: \(ReferenceObject/
		}
	},

	'services_adtr'	=> {
		'sections'	=> {
			'referenceobject'	=> qr/: \(ReferenceObject/
		}
	},
	
	'src_adtr_translated'	=> {
		'sections'	=> {
			'referenceobject'	=> qr/: \(ReferenceObject/,
			'admininfo_adtr'	=> qr/:AdminInfo \($/
		}
	},
	
	'dst_adtr_translated'	=> {
		'sections'	=> {
			'referenceobject'	=> qr/: \(ReferenceObject/,
			'admininfo_adtr'	=> qr/:AdminInfo \($/
		}
	},
	
	'services_adtr_translated'	=> {
		'sections'	=> {
			'referenceobject'	=> qr/: \(ReferenceObject/,
			'admininfo_adtr'	=> qr/:AdminInfo \($/
		}
	},
	
	'admininfo_adtr'	=> {
		'data'	=> {
			'class'		=> qr/:ClassName \((service_translate|translate_static|translate_hide)\)/
		}
	},

	'track'	=> {
		'sections'	=> {
			'referenceobject'	=> qr/: \(ReferenceObject/
		}
	},
	
	'install'	=> {
		'sections'	=> {
			'referenceobject'	=> qr/: \(ReferenceObject/
		}
	},
	
	'through'	=> {
		'sections'	=> {
			'referenceobject'	=> qr/: \(ReferenceObject/
		}
	},

	'referenceobject'	=> {
		'data'	=> {
			'reference'	=> qr/:Name \((.*)\)/,
			'table'		=> qr/:Table \(([a-zA-Z0-9_]+)\)/
		}
	},
	
	'action'	=> {
		'sections'	=> {
			'actionobj'			=> qr/: \((accept|drop|reject)/
		}
	},
	
	'actionobj'	=> {
		'sections'	=> {
			'admininfo_act'		=> qr/:AdminInfo \($/,
			'identity_settings'	=> qr/:identity_settings \(/	# Dummy definition, just to hide another 'admininfo' section inside.
		}		
	},
	
	'admininfo_act'	=> {
		'data'	=> {
			'class'		=> qr/:ClassName \((drop_action|accept_action|reject_action)\)/
		}
	}

);

# Data structure used to validate objects and rules syntax.
# 
# Syntax: [<data_type>,<data_regex>, {<options>}]
#
#   - data_type: scalar/reference,
#   - data_regex: qr/.../ (scalar), [qr/<keys>/, qr/<values>/] (references),
#   - options:
#     * 'if'		=> ['param_name', qr/.../]		Check parameter when condition is met; undefined value means that the current param. is optional,
#     * 'keys'		=> ['key1', 'keys2', ...]		Check for mandatory keys in a section (reference data type).
#     * 'minkeys'	=> <number>						Minimal number of keys in particular reference section.
#     * 'maxkeys'	=> <number>						Maximum number of keys in particular reference section.
#
my %validate = (
	
	'network'				=> {
		'ipaddr'				=> ['scalar', qr/[0-9]+.[0-9]+.[0-9]+.[0-9]+/],
		'netmask'				=> ['scalar', qr/[0-9]+.[0-9]+.[0-9]+.[0-9]+/],
		'add_adtr_rule'			=> ['scalar', qr/true|false/],
		'netobj_adtr_method'	=> ['scalar', qr/adtr_hide|adtr_static/, {'if' => ['add_adtr_rule', qr/true/]}],
		'valid_ipaddr'			=> ['scalar', qr/[0-9]+.[0-9]+.[0-9]+.[0-9]+/, {'if' => ['add_adtr_rule', qr/true/]}],
		'the_firewalling_obj'	=> ['scalar', qr/.*/, {'if' => ['add_adtr_rule', qr/true/]}]
	},
	
	'host_plain'			=> {
		'ipaddr'				=> ['scalar', qr/[0-9]+.[0-9]+.[0-9]+.[0-9]+/],
		'add_adtr_rule'			=> ['scalar', qr/true|false/],
		'netobj_adtr_method'	=> ['scalar', qr/adtr_hide|adtr_static/, {'if' => ['add_adtr_rule', qr/true/]}],
		'valid_ipaddr'			=> ['scalar', qr/[0-9]+.[0-9]+.[0-9]+.[0-9]+/, {'if' => ['add_adtr_rule', qr/true/]}],
		'the_firewalling_obj'	=> ['scalar', qr/.*/, {'if' => ['add_adtr_rule', qr/true/]}]
	},
	
	'network_object_group'	=> {
		'reference'				=> ['reference', [qr/.*/, qr/network_objects/]]
	},
	
	'group_with_exception'	=> {
		'base'					=> ['scalar', qr/.*/],
		'exception'				=> ['scalar', qr/.*/]
	},
	
	'address_range'			=> {
		'ipaddr_first'			=> ['scalar', qr/[0-9]+.[0-9]+.[0-9]+.[0-9]+/],
		'ipaddr_last'			=> ['scalar', qr/[0-9]+.[0-9]+.[0-9]+.[0-9]+/],
		'add_adtr_rule'			=> ['scalar', qr/true|false/],
		'netobj_adtr_method'	=> ['scalar', qr/adtr_hide|adtr_static/, {'if' => ['add_adtr_rule', qr/true/]}],
		'valid_ipaddr'			=> ['scalar', qr/[0-9]+.[0-9]+.[0-9]+.[0-9]+/, {'if' => ['add_adtr_rule', qr/true/]}],
		'the_firewalling_obj'	=> ['scalar', qr/.*/, {'if' => ['add_adtr_rule', qr/true/]}]
	},
	
	'tcp_service'			=> {
		'port'					=> ['scalar', qr/[0-9]+/]
	},
	
	'udp_service'			=> {
		'port'					=> ['scalar', qr/[0-9]+/]
	},
	
	'icmp_service'			=> {
		'icmp_type'				=> ['scalar', qr/[0-9]+/]
	},
	
	'other_service'			=> {
		'protocol'				=> ['scalar', qr/[0-9]+/]
	},
	
	'dcerpc_service'		=> {
		'uuid'					=> ['scalar', qr/[a-zA-Z0-9\-]+/]
	},
	
	'service_group'			=> {
		'reference'				=> ['reference', [qr/.*/, qr/services/]]
	},

	'security_rule'			=> {
		'disabled'				=> ['scalar', qr/true|false/],
		'src'					=> ['reference', [qr/.*/, qr/network_objects|globals|\'\'|"not in"/], {'keys' => ['_op'], 'minkeys' => 2}],
		'dst'					=> ['reference', [qr/.*/, qr/network_objects|globals|\'\'|"not in"/], {'keys' => ['_op'], 'minkeys' => 2}],
		'through'				=> ['reference', [qr/.*/, qr/communities|globals/], {'minkeys' => 1}],
		'services'				=> ['reference', [qr/.*/, qr/services|globals|\'\'|"not in"/], {'keys' => ['_op'], 'minkeys' => 2}],
		'action'				=> ['reference', [qr/.*/, qr/_action/], {'minkeys' => 1}],
		'track'					=> ['reference', [qr/.*/, qr/tracks/], {'minkeys' => 1}],
		'install'				=> ['reference', [qr/.*/, qr/network_objects|globals|setup/], {'minkeys' => 1}]
	},
		
	'address_translation_rule'	=> {
		'disabled'					=> ['scalar', qr/true|false/],
		'src_adtr'					=> ['reference', [qr/.*/, qr/network_objects|globals/], {'minkeys' => 1, 'maxkeys' => 1}],
		'dst_adtr'					=> ['reference', [qr/.*/, qr/network_objects|globals/], {'minkeys' => 1, 'maxkeys' => 1}],
		'services_adtr'				=> ['reference', [qr/.*/, qr/services|globals/], {'minkeys' => 1, 'maxkeys' => 1}],
		'src_adtr_translated'		=> ['reference', [qr/.*/, qr/network_objects|globals|translate_/], {'keys' => ['_translate'], 'minkeys' => 2, 'maxkeys' => 2}],
		'dst_adtr_translated'		=> ['reference', [qr/.*/, qr/network_objects|globals|translate_/], {'keys' => ['_translate'], 'minkeys' => 2, 'maxkeys' => 2}],
		'services_adtr_translated'	=> ['reference', [qr/.*/, qr/services|globals|service_translate/], {'keys' => ['_translate'], 'minkeys' => 2, 'maxkeys' => 2}],
		'install'					=> ['reference', [qr/.*/, qr/network_objects|globals|setup/], {'minkeys' => 1}]
	},
	
	'security_header_rule'	=> {
		'disabled'				=> ['scalar', qr/true/],
		'src'					=> ['reference', [qr/Any|_op/, qr/globals|\'\'/], {'keys' => ['Any', '_op'], 'minkeys' => 2, 'maxkeys' => 2}],
		'dst'					=> ['reference', [qr/Any|_op/, qr/globals|\'\'/], {'keys' => ['Any', '_op'], 'minkeys' => 2, 'maxkeys' => 2}],
		'services'				=> ['reference', [qr/Any|_op/, qr/globals|\'\'/], {'keys' => ['Any', '_op'], 'minkeys' => 2, 'maxkeys' => 2}],
		'action'				=> ['reference', [qr/drop/, qr/drop_action/], {'minkeys' => 1, 'maxkeys' => 1}],
		'track'					=> ['reference', [qr/None/, qr/tracks/], {'minkeys' => 1, 'maxkeys' => 1}],
		'install'				=> ['reference', [qr/.*/, qr/.*/], {'minkeys' => 1}],
		'header_text'			=> ['scalar', qr/.*/],
		'state'					=> ['scalar', qr/expanded|collapsed/]
	},
	
	'nat_header_rule'		=> {
		'disabled'				=> ['scalar', qr/true/],
		'src'					=> ['reference', [qr/Any|_op/, qr/globals|\'\'/], {'keys' => ['Any', '_op'], 'minkeys' => 2, 'maxkeys' => 2}],
		'dst'					=> ['reference', [qr/Any|_op/, qr/globals|\'\'/], {'keys' => ['Any', '_op'], 'minkeys' => 2, 'maxkeys' => 2}],
		'services'				=> ['reference', [qr/Any|_op/, qr/globals|\'\'/], {'keys' => ['Any', '_op'], 'minkeys' => 2, 'maxkeys' => 2}],
		'action'				=> ['reference', [qr/drop/, qr/drop_action/], {'minkeys' => 1, 'maxkeys' => 1}],
		'track'					=> ['reference', [qr/None/, qr/tracks/], {'minkeys' => 1, 'maxkeys' => 1}],
		'install'				=> ['reference', [qr/.*/, qr/.*/], {'minkeys' => 1}],
		'header_text'			=> ['scalar', qr/.*/],
		'state'					=> ['scalar', qr/expanded|collapsed/]
	}
	
);




#####################################################################################
# Subroutines for internal use only
#####################################################################################


#
# readline($line, $ref_def, $ref_stack)
#
# Extracts and returns data from a single line of a Check Point configuration file.
# Modifies the 'stack' variable whenever a section is entered of left. 
#
# Parameters:
#
#   - $line 		Next config line,
#   - $ref_def		Reference to the varaible defining a structure of the read file,
#   - $ref_stack	Reference to the 'stack' array.
#
# Returns a hash reference:
#
#   {
#     'section_start'	=> array ['type', 'realname'] if a line is a section start, or undef in other cases,
#     'section_end'		=> scalar 'type' if a line is a section end, or undef in other cases,
#     'data'			=> array ['name', 'value'] if a line contains any valid data, or undef in other cases
#   }
#
 
sub readline {

	my($line, $ref_def, $ref_stack) = @_;				# Capture parameters.
	my $ref_current = $ref_def->{$ref_stack->[-1]};	# Reference to the current section (based on the current stack status).
	
	# Remove line endings and all non-ASCII characters.
	chomp $line;
	$line =~ s/[^[:ascii:]]//g;
	
	# Returned data structure.
	my %return = (
		'section_start'	=> undef,		# Section start: ('type', 'realname').
		'section_end'	=> undef,		# Section end: 'type'.
		'data'			=> undef		# Data: ('name', 'value').
	);

	# Look for the start of a new section.
	if (exists $ref_current->{'sections'}) {
		foreach (keys %{$ref_current->{'sections'}}) {
			if ($line =~ $ref_current->{'sections'}{$_}) {
				$return{'section_start'} = [ $_, $1 ];
				push @$ref_stack, $_;
				$ref_def->{$_}{'counter'} = 1;
				return \%return;
			}
		}
	}
	
	# Look for data withing the current section.
	if (exists $ref_current->{'data'}) {
		foreach (keys %{$ref_current->{'data'}}) {
			if ($line =~ $ref_current->{'data'}{$_}) {
				$return{'data'} = [$_, $1];
				return \%return;
			}
		}
	}
	
	# End section
	if ($ref_stack->[-1] ne 'MAIN') {
		$ref_current->{'counter'}++ if $line =~ /\(/;
		$ref_current->{'counter'}-- if $line =~ /\)/;
		if ($ref_current->{'counter'} == 0) {
			$return{'section_end'} = pop @$ref_stack;
			return \%return;
		}
	}
	
	return \%return;

}





#####################################################################################
# Subroutines
#####################################################################################


#
# cpdb::read($ref_params)
#
# Reads configuration from Check Point files and returns a data structure with the config.
# Can read both objects_5_0.C and rulebases_5_0.fws file. At least one file must be specified
# in the parameters hash.
#
# Parameters (all optional):
#
#   $ref_params = {
#     'objects'		=> 'path to objects_5_0.C file',
#     'rulebases'	=> 'path to rulebases_5_0.fws file',
#     'policypkg'	=> 'Policy Package name' (if not specified, all will be read)
#   }
#
# Returns a hash reference:
#
#   $VAR = {
#     'network_objects'	=> {
#       'object1' => {
#         'param1' => 'value1',
#         'param2' => 'value2',
#         'nested' => {
#           'param3' => 'value3',
#           'param4' => 'value4'
#         }
#       },
#       'object2' => {...}
#     },
#     'services'	=> {...},
#     'globals'		=> {...},
#     'rulebases'	=> {
#       'pkgname1'	=> {
#         'rule'		=> [{%rule1}, {%rule2}, {%rule3}],
#         'rule_adtr'	=> [{%nat_rule1}, {%nat_rule2}, {%nat_rule3}]
#       },
#       'pkgname2'	=> {...}
#     }
#   };
# 

sub read {

	my($ref_params) = @_;	# Subroutine parameters (expected hash reference with 'objects' and/or 'rules' entries).
	my %cpdb;				# Data structure containing all information read from all Check Point config files.
	
	# Read Check Point objects.
	if (defined $ref_params->{'objects'}) {
	
		my @object;		# Data structure containing a single object before it is written in the %cpdb.
		my %nested;		# Hash containing information for nested entries (e.g. references).
		
		# Open Check Point configuration file.
		open OBJECTS, "<$ref_params->{'objects'}" or die "Can't open Check Point objects file: $!";

		# Read from a file and save objects configuration.
		while (<OBJECTS>) {
			
			# Read line.
			my $ref_read = &readline($_, \%objdef, \@stack);
	
			# Actions when a section starts.
			if (defined $ref_read->{'section_start'}) {
				$object[0] = $ref_read->{'section_start'}[1] if $ref_read->{'section_start'}[0] eq 'object';	# Start of the 'object' section.
			}
	
			# Actions when data is found in the processed line.	
			if (defined $ref_read->{'data'}) {
				if ($stack[-1] =~ /referenceobject|the_firewalling_obj|proto_type|base|exception/) {		# Sections that need collecting multiple parameters for a single entry.
					$nested{$ref_read->{'data'}[0]} = $ref_read->{'data'}[1];
				} else {	# Other parameters.
					$object[1]{$ref_read->{'data'}[0]} = $ref_read->{'data'}[1];
				}
			}
	
			# Actions when a section ends.
			if (defined $ref_read->{'section_end'}) {
				if ($ref_read->{'section_end'} =~ /the_firewalling_obj|proto_type|base|exception/) {
					$object[1]{$ref_read->{'section_end'}} = "$nested{'table'}:$nested{'name'}";
					%nested = ();
				}
				if ($ref_read->{'section_end'} eq 'referenceobject') {
					$object[1]{'reference'}{$nested{'reference'}} = $nested{'table'};
					%nested = ();
				}
				if ($ref_read->{'section_end'} eq 'object') {	# End of the 'object' section - save data.
					$cpdb{$stack[-1]}{$object[0]} = $object[1] if exists $object[1]{'class'};
					@object = ();
				}
			}
		
		}

		# Close opened files.
		close OBJECTS;
	
	}
	
	# Read Check Point rulebases.
	if (defined $ref_params->{'rulebases'}) {
	
		my $pkgname;	# Policy Package name.
		my $actname;	# Action name.
		my $ruletype;	# Type of a processed rule.
		my %nested;		# Hash containing information for nested entries (e.g. references).
		my %rule;		# Hash containing information about a single rule.
		
		# Open Check Point configuration file.
		open RULEBASES, "<$ref_params->{'rulebases'}" or die "Can't open Check Point rulebases file: $!";
		
		# Read from a file and save objects configuration.
		while (<RULEBASES>) {
			
			# Read line.
			my $ref_read = &readline($_, \%ruldef, \@stack);

			# Actions when a section starts.
			if (defined $ref_read->{'section_start'}) {
				$pkgname = $ref_read->{'section_start'}[1] if $ref_read->{'section_start'}[0] eq 'rulebase';	# Start of the 'rulebase' section.
				$actname = $ref_read->{'section_start'}[1] if $ref_read->{'section_start'}[0] eq 'actionobj';	# Start of the 'actionobj' section.
				$ruletype = $ref_read->{'section_start'}[1] if $ref_read->{'section_start'}[0] eq 'ruleobj';	# Start of the 'ruleobj' section.
			}
			
			# Actions when data is found in the processed line.
			if (defined $ref_read->{'data'}) {
				if ($stack[-1] =~ /referenceobject|admininfo_adtr|admininfo_act/) {	# Collect data within a 'referenceobject', 'admininfo_adtr' or 'admininfo_act' section.
					$nested{$ref_read->{'data'}[0]} = $ref_read->{'data'}[1];
				} elsif ($ref_read->{'data'}[0] eq 'op') {							# Collect 'op' paramaters.	
					$rule{$stack[-1]}{'_op'} = ($ref_read->{'data'}[1]) ? $ref_read->{'data'}[1] : "''";
				}else {																# Collect data for any other sections.
					$rule{$ref_read->{'data'}[0]} = $ref_read->{'data'}[1];
				}
			}
			
			# Actions when a section ends.
			if (defined $ref_read->{'section_end'}) {
				if ($ref_read->{'section_end'} eq 'admininfo_act') {		# Write data from a single 'admininfo_act' section.
					$rule{'action'}{$actname} = $nested{'class'};
					%nested = ();					
				}
				if ($ref_read->{'section_end'} eq 'admininfo_adtr') {		# Write data from a single 'admininfo_adtr' section.
					$rule{$stack[-1]}{'_translate'} = $nested{'class'};
					%nested = ();					
				}
				if ($ref_read->{'section_end'} eq 'referenceobject') {		# Write data from a single 'referenceobject' section.	
					$rule{$stack[-1]}{$nested{'reference'}} = $nested{'table'};
					%nested = ();
				}
				if ($ref_read->{'section_end'} eq 'ruleobj') {	# End of the 'rule'/'rule_adtr' section - save a single rule.
					if (exists $ref_params->{'policypkg'}) {
						push @{$cpdb{'rulebases'}{$pkgname}{$ruletype}}, {%rule} if $ref_params->{'policypkg'} eq $pkgname;
					} else {
						push @{$cpdb{'rulebases'}{$pkgname}{$ruletype}}, {%rule};
					}
					%rule = ();
				}
			}
			
			
		}
		
		# Close opened files.
		close RULEBASES;
		
	}
	
	return \%cpdb;

}


#
# cpdb::check($ref_entry)
#
# Checks objects and rules if they have the correct syntax and necessary parameters.
#
# Parameters:
#
#   - $ref_entry	Reference to an object or a rule.
#
# Returns an array reference with a list of problematic parameters. If everything is fine, the array is empty.
#

sub check {

	my($ref_entry) = @_;	# Capture subroutine parameters.
	my @missing = ();		# Array containing problematic parameters for specific entry.
	
	
	# Check if 'class' exists and is defined in the %validate hash.
	if (exists $ref_entry->{'class'} && exists $validate{$ref_entry->{'class'}}) {
	
		my $class = $ref_entry->{'class'};
		
		# Check all defined parameters in the %validate hash for the aquired class.
		foreach my $param (keys %{$validate{$class}}) {
		
			my $type		= $validate{$class}{$param}[0];	# Value type.
			my $regex		= $validate{$class}{$param}[1];	# Regex reference.
			my $ref_options	= (defined $validate{$class}{$param}[2]) ? $validate{$class}{$param}[2] : undef;	# Additional options.
			
			# Entry contains a parameter. 
			if (exists $ref_entry->{$param}) {
			
				# Value is a reference .
				if (ref($ref_entry->{$param}) && $type eq 'reference') {
				
					my $minkeyscnt = (exists $ref_options->{'minkeys'}) ? $ref_options->{'minkeys'} : 0;			# Key counter set to minimal number of keys required (default 0).
					my $maxkeyscnt = (exists $ref_options->{'maxkeys'}) ? $ref_options->{'maxkeys'} : undef;		# Key counter set to maximal number of keys required (default undef).
					
					my($keyregex, $valregex) = @$regex;	# Hash key regex, hash value regex.
					my @temp_missing = ();					# Temporary hash containing invalid parameters.
					
					# Handle 'keys' option. Check required keys.
					if (exists $ref_options->{'keys'}) {
						foreach (@{$ref_options->{'keys'}}) {
							push @temp_missing, "$param:$_(mandatory)" if ! exists $ref_entry->{$param}{$_};
						}
					}
					
					# Check all hash keys and values agains regex-es.
					foreach my $subparam (keys %{$ref_entry->{$param}}) {
						push @temp_missing, "$param:$subparam(regex)" if ! ($subparam =~ $keyregex && $ref_entry->{$param}{$subparam} =~ $valregex);
						$minkeyscnt--;
						$maxkeyscnt-- if defined $maxkeyscnt;
					}
					push @temp_missing, "$param(minkeys)" if $minkeyscnt > 0;
					push @temp_missing, "$param(maxkeys)" if defined $maxkeyscnt && $maxkeyscnt < 0;

					# Handle 'if' option. 
					if (exists $ref_options->{'if'} && defined $ref_options->{'if'}) {
						my($ifparam, $ifregex) = @{$ref_options->{'if'}};		# Parameter name, value regex.
						if (exists $ref_entry->{$ifparam} && $ref_entry->{$ifparam} =~ $ifregex) {
							# Conditional param. exists and has proper value - push all captured errors so far.
							push @missing, @temp_missing;
						} else { push @missing, "$param(if)" }	# Conditional param. not exists or has wrong value - write this param to the error array.
					} else { push @missing, @temp_missing }	# No 'if' parameter, release all captured errors.

				# Value is a scalar.
				} elsif (! ref($ref_entry->{$param}) && $type eq 'scalar') {
				
					my @temp_missing = ();		# Temporary hash containing invalid parameters.
					
					# Check parameter value against declared regex.
					push @temp_missing, "$param(regex)" if ! ($ref_entry->{$param} =~ $regex);
				
					# Handle 'if' option. 
					if (exists $ref_options->{'if'} && defined $ref_options->{'if'}) {
						my($ifparam, $ifregex) = @{$ref_options->{'if'}};		# Parameter name, value regex.
						if (exists $ref_entry->{$ifparam} && $ref_entry->{$ifparam} =~ $ifregex) {
							# Conditional param. exists and has proper value - push all captured errors so far.
							push @missing, @temp_missing;
						} else { push @missing, "$param(if)" }	# Conditional param. not exists or has wrong value - write this param to the error array.
					} else { push @missing, @temp_missing }	# No 'if' parameter, release all captured errors.

				# Value type does not match declaration.
				} else { push @missing, "$param(type)" }
				
			# Entry does not contain a parameter. 
			} else {
			
				# Handle 'if' option. Check if it is allowed that the param does's exist.
				if (exists $ref_options->{'if'}) {
					# Option 'if' has a defined value (param/regex list). Check agains that value.
					# If option 'if' doesn't have a defined value, parameter is considered as optional.
					if (defined $ref_options->{'if'}) {
						my($ifparam, $ifregex) = @{$ref_options->{'if'}};		# Parameter name, value regex.
						# Parameter doesn't exist but it should - write it to the error array.
						push @missing, "$param(if)" if exists $ref_entry->{$ifparam} && $ref_entry->{$ifparam} =~ $ifregex;
					}
				} else { push @missing, "$param(missing)" }	# No 'if' option - parameter is mandatory but not exists - write this param to the error array.
			
			}
		
		}
	
	} else { push @missing, 'class' }		# Class doesn't exist or not supported.
	
	# Return a list of missing or incorrect parameters.
	return \@missing;
	
}



#
# cpdb::dbedit($id, $ref_entry, $ref_options)
#
# Prints information about particular objects or rules as a set of dbedit commands.
# Output can be directly used for making changes in the Check Point configuration,
# using dbedit tools.
#
# Mandatory parameters:
#
#   - $id			Object name or rule number to be printed.
#   - $ref_entry	Reference to a specific object or rule.
#
# Optional parameters:
#
#   $ref_options = {
#     'policypkg'	=> 'Name of the Policy Package to be used for rules ('Standard' by default)',
#     'replace'		=> 'omit_old'/'replace_only',	# 'omit_old' - replaced objects are omitted in the dbedit config; 
#                                                   # 'replace_only' - only replacement commands are printed; it is assumed that modified objects/rules already exist.
#     'cmp'			=> {	# Reference to a compare hash containing objects and their replacements.
#       'object_type1'	=> {
#         'object1'	=> 'replacing_object',
#         'object2'	=> 'replacing_object'
#       },
#       'object_type2'	=> {
#         'object3'	=> 'replacing_object',
#         'object4'	=> 'replacing_object'
#       } 
#     }
#   }
#
# Returns an array reference with dbedit commands (one command per array element).
#

sub dbedit {

	my($id, $ref_entry, $ref_options) = @_;		# Capture subroutine parameters.
	
	my $pkgname = (exists $ref_options->{'policypkg'}) ? $ref_options->{'policypkg'} : 'Standard';	# Name of a Policy Package to be used when creating rules (default - 'Standard').
	my $replace = (exists $ref_options->{'replace'}) ? $ref_options->{'replace'} : 0;				# Replacement option ('omit_old'/'replace_only'), default is 0.
	my $ref_cmp = (exists $ref_options->{'cmp'}) ? $ref_options->{'cmp'} : {};						# Reference to a compare hash, by default set to an empty hash ref.
	
	my @dbedit = ();	# Array containing lines in the dbedit format.
	my $type;			# Rule/object type ('network_objects', 'services', 'rule', 'rule_adtr')

	# Get the type of an entry. If not possible, return empty list reference.
	if (exists $ref_entry->{'class'} && exists $types{$ref_entry->{'class'}}) {
		$type = $types{$ref_entry->{'class'}};
	} else {
		return \@dbedit;
	}
	
	# Objects.
	if ($type =~ /services|network_objects/) {
	
		if (exists $ref_cmp->{$type}{$id} && $replace =~ /omit_old|replace_only/) {		# Replacements options set, replacement for an object exists - add comment only.
		
			push @dbedit , "## Object '$id' is replaced by '$ref_cmp->{$type}{$id}'";
			
		} else {	# No replacement for an object - proceed with adding dbedit commands.
		
			# Create an object (only if 'replace_only' option is not used).
			push @dbedit, "create $ref_entry->{'class'} $id" if (exists $ref_entry->{'class'} && $replace ne 'replace_only');
		
			# Modify or add elements.
			foreach my $param (keys %$ref_entry) {
				if ($param eq 'reference') {	# Process references ('addelement' entries).
					foreach my $refobj (keys %{$ref_entry->{'reference'}}) {
						if (exists $ref_cmp->{$type}{$refobj} && $replace =~ /omit_old|replace_only/) {		# Replacing object exists, replacement options set - replace an object.
							push @dbedit, "## Replacing '$refobj' with '$ref_cmp->{$type}{$refobj}' ##";
							push @dbedit, "rmelement $type $id '' $type:$refobj" if $replace eq 'replace_only';	# If 'replace_only' option is used, also remove the old object.
							push @dbedit, "addelement $type $id '' $type:$ref_cmp->{$type}{$refobj}";
						} else {
							# Add other references, only if 'replace_only' is not used.
							push @dbedit, "addelement $type $id '' $type:$refobj" if $replace ne 'replace_only';
						}
					}
				} elsif (not($param =~ /class|global|valid_ipaddr|netobj_adtr_method|the_firewalling_obj/)) {	# Process all other data ('modify' entries), expect some params.
					# Add entries only when: values are not empty, 'replace_only' option is not used.
					push @dbedit, "modify $type $id $param $ref_entry->{$param}" if ($ref_entry->{$param} ne '' && $replace ne 'replace_only');
				}
			}
			
			# Optional automatic NAT for 'network_objects'.
			if (exists $ref_entry->{'add_adtr_rule'} && $ref_entry->{'add_adtr_rule'} eq 'true' && $replace ne 'replace_only') {
				push @dbedit, "modify network_objects $id NAT NAT";
				foreach ('netobj_adtr_method', 'valid_ipaddr', 'valid_ipaddr6', 'the_firewalling_obj') {
					push @dbedit, "modify network_objects $id NAT:$_ $ref_entry->{$_}" if exists $ref_entry->{$_};
				}
			}
		
			# Update an object.
			if (@dbedit) {		# Add 'update' command only when there are changes (other dbedit commands) for an object.
				push @dbedit, "update $type $id";
			} else {			# Add a comment if there are no changes (no dbedit commands) for an object.
				push @dbedit, "## No changes in object '$id' ##";
			}
			
		}
		
	}
	
	# Rules.
	if ($type =~ /(rule|rule_adtr)$/) {
		
		# Create a rule (only if 'replace_only' option is not used).
		push @dbedit, "addelement fw_policies ##$pkgname $type $ref_entry->{'class'}" if $replace ne 'replace_only';
		
		# Rule name, comments, disabled status and block number (only if 'replace_only' option is not used).
		foreach ('name', 'comments', 'header_text', 'state', 'disabled', 'rule_block_number') {
			# Add entries only when parameters are not empty and 'replace_only' option is not used.
			push @dbedit, "modify fw_policies ##$pkgname $type:$id:$_ $ref_entry->{$_}" if (exists $ref_entry->{$_} && $ref_entry->{$_} ne '' && $replace ne 'replace_only');
		}
		
		# Tracking and action (only if 'replace_only' option is not used).
		foreach ('track', 'action') {
			if (exists $ref_entry->{$_}) {
				my $entry = (keys %{$ref_entry->{$_}})[0];
				push @dbedit, "rmbyindex fw_policies ##$pkgname $type:$id:$_ 0" if ($_ eq 'track' && $replace ne 'replace_only');
				push @dbedit, "addelement fw_policies ##$pkgname $type:$id:$_ $ref_entry->{$_}{$entry}:$entry" if $replace ne 'replace_only';				
			}
		}
		
		# 'Install on' field (only if 'replace_only' option is not used).
		foreach (keys %{$ref_entry->{'install'}}) {
			my $fwonly = ($ref_entry->{'class'} eq 'address_translation_rule') ? "" : ":''";		# Remove string (:'') from NAT rules.
			push @dbedit, "addelement fw_policies ##$pkgname $type:$id:install$fwonly $ref_entry->{'install'}{$_}:$_" if $replace ne 'replace_only';
		}
		
		# Sections that use 'addelement' format.
		foreach my $section ('src', 'dst', 'services', 'through', 'src_adtr', 'dst_adtr', 'services_adtr') {
			if (exists $ref_entry->{$section}) {
				# Write the 'op' option. Commands needed only for regular rules.
				push @dbedit, "modify fw_policies ##$pkgname $type:$id:$section:op $ref_entry->{$section}{'_op'}" if (not($section =~ /_adtr|through/) && $replace ne 'replace_only');
				foreach (keys %{$ref_entry->{$section}}) {
					next if $_ =~ /^_/;						# Omit all special parameters (beginning with '_').
					my $objtype = $ref_entry->{$section}{$_};	# Get object type.
					my $fwonly = ($section =~ /_adtr/) ? "" : ":''";	# Additional string (:'') needed only for regular rules.
					if (exists $ref_cmp->{$objtype}{$_} && $replace =~ /omit_old|replace_only/) {	# Replacing object exists, replacement options set - replace an object.
						push @dbedit, "## Replacing '$_' with '$ref_cmp->{$objtype}{$_}' ##";
						push @dbedit, "rmelement fw_policies ##$pkgname $type:$id:$section$fwonly $objtype:$_" if $replace eq 'replace_only';	# If 'replace_only' option is used, also remove the old object.
						push @dbedit, "addelement fw_policies ##$pkgname $type:$id:$section$fwonly $objtype:$ref_cmp->{$objtype}{$_}";
					} else {
						push @dbedit, "addelement fw_policies ##$pkgname $type:$id:$section$fwonly $objtype:$_" if $replace ne 'replace_only';
					}
				}
			}
		}
		
		# Sections that use 'modify' format.
		foreach my $section ('src_adtr_translated', 'dst_adtr_translated', 'services_adtr_translated') {
			if (exists $ref_entry->{$section}) {
				# Special entry defyining NAT type.
				push @dbedit, "modify fw_policies ##$pkgname $type:$id:$section $ref_entry->{$section}{'_translate'}" if $replace ne 'replace_only';
				# Other entries.
				foreach (keys %{$ref_entry->{$section}}) {
					next if $_ =~ /^_/;	# Omit all special parameters (beginning with '_').
					my $objtype = $ref_entry->{$section}{$_};	# Get object type.
					if (exists $ref_cmp->{$objtype}{$_} && $replace =~ /omit_old|replace_only/) {	# Replacing object exists, replacement options set - replace an object.
						push @dbedit, "## Replacing '$_' with '$ref_cmp->{$objtype}{$_}' ##";
						push @dbedit, "modify fw_policies ##$pkgname $type:$id:$section:'' $objtype:$ref_cmp->{$objtype}{$_}";
					} else {
						push @dbedit, "modify fw_policies ##$pkgname $type:$id:$section:'' $objtype:$_" if $replace ne 'replace_only';
					}
				}				
			}
		}
		
		# Update a rule.
		if (@dbedit) {		# Add 'update' command if there are any changes (dbedit commands) for a rule.
			push @dbedit, "update fw_policies ##$pkgname";
		} else {			# Add a comment if there are no changes (no dbedit commands) for a rule.
			push @dbedit, "## No changes in rule '$id' ##";
		}
		
	}
	
	return \@dbedit;
}


#
# cpdb::dbedit_sectitle($type, $id, $title, $ref_options)
#
# Prints a Section Title both for security and NAT rules in the dbedit format.
# By default the 'Standard' Policy Package is used.
#
# Mandatory parameters:
#
#   - $type			Rule type ('rule', 'rule_adtr')
#   - $id			Rule number to be printed.
#   - $title		Section Title text.
#
# Optional parameters:
#
#   $ref_options = {
#     'policypkg'	=> 'Name of the Policy Package to be used for a Section Title ('Standard' by default)',
#   }
#
# Returns an array reference with dbedit commands (one command per array element).
#

sub dbedit_sectitle {

	my($type, $id, $title, $ref_options) = @_;		# Capture subroutine parameters.
	
	my $pkgname = (exists $ref_options->{'policypkg'}) ? $ref_options->{'policypkg'} : 'Standard';	# Name of a Policy Package to be used when creating rules (default - 'Standard').
	
	my @dbedit;		# Array containing lines in the dbedit format.
	
	my %class = (	# Hash resolving rule type into a proper class.
		'rule'		=> 'security_header_rule',
		'rule_adtr'	=> 'nat_header_rule'
	);
	
	# Print Section Title.
	push @dbedit, "addelement fw_policies ##$pkgname $type $class{$type}";
	push @dbedit, "modify fw_policies ##$pkgname $type:$id:header_text \"$title\"";
	push @dbedit, "modify fw_policies ##$pkgname $type:$id:state collapsed";
	push @dbedit, "modify fw_policies ##$pkgname $type:$id:disabled true";
	push @dbedit, "rmbyindex fw_policies ##$pkgname $type:$id:track 0";
	push @dbedit, "addelement fw_policies ##$pkgname $type:$id:track tracks:None";
	push @dbedit, "addelement fw_policies ##$pkgname $type:$id:action drop_action:drop";
	push @dbedit, "addelement fw_policies ##$pkgname $type:$id:install:'' globals:Any";
	push @dbedit, "modify fw_policies ##$pkgname $type:$id:src:op ''";
	push @dbedit, "addelement fw_policies ##$pkgname $type:$id:src:'' globals:Any";
	push @dbedit, "modify fw_policies ##$pkgname $type:$id:dst:op ''";
	push @dbedit, "addelement fw_policies ##$pkgname $type:$id:dst:'' globals:Any";
	push @dbedit, "modify fw_policies ##$pkgname $type:$id:services:op ''";
	push @dbedit, "addelement fw_policies ##$pkgname $type:$id:services:'' globals:Any";
	push @dbedit, "update fw_policies ##$pkgname";
	
	return \@dbedit;
}


#
# cpdb::objused($ref_entrylist, $ref_options)
#
# Looks for objects used in rules or groups and prints their number of occurrences
#
# Mandatory parameters:
#
#   - $ref_entrylist = [$ref_entry1, $ref_entry2, ...]	List containing references to particular group objects or rules (can be mixed together).
#
# Optional parameters:
#
#   $ref_options = {
#     'recursive'	=> $ref_objdb	Reference to a hash containing objects definitions (created by the 'read' subroutine).
#   }
#
# Returns a hash reference:
#
#   $VAR = {
#     'object_type1'	=> {
#       'object1'	=> number_of_occurrences,
#       'object2'	=> number_of_occurrences
#     },
#     'object_type2'	=> {
#       'object3'	=> number_of_occurrences,
#       'object4'	=> number_of_occurrences
#     }
#   };
#

sub objused {

	my($ref_entrylist, $ref_options) = @_;		# Capture subroutine parameters.
	
	my %objused;		# Hash containing used objects.
	my %groupsused;		# Hash with used groups to verify their content.
	my %groupschecked;	# Hash with groups with already checked content.
	
	my @sections = (	# List of sections to be checked.
		'reference',
		'src',
		'dst',
		'services',
		'through',
		'src_adtr',
		'dst_adtr',
		'services_adtr',
		'src_adtr_translated',
		'dst_adtr_translated',
		'services_adtr_translated',
		'install'
	);
	
	# Collect objects directly used in rulebases
	foreach my $ref_entry (@$ref_entrylist) {
		foreach my $section (@sections) {
			if (exists $ref_entry->{$section}) {
				foreach my $obj (keys %{$ref_entry->{$section}}) {
					next if $obj =~ /^_/;		# Omit all special parameters (beginning with '_').
					my $type = $ref_entry->{$section}{$obj};
					$objused{$type}{$obj}++;	# Write the number of occurrences of an object.
					# Recursive option is set. Save groups in a temporary hash ($groupsused) for further recursive check.
					# Omit 'communities' type to avoid creating the section in the db structure.
					if (not($type =~ /communities/) && exists $ref_options->{'recursive'}{$type}{$obj}) {
						$groupsused{$obj} = $type if $ref_options->{'recursive'}{$type}{$obj}{'class'} =~ /_group|group_with_exception/;					
					}
				}
			}
		}
	}
	
	
	# Look for additional objects in groups.
	while (%groupsused) {	# Stop processing when there are no more groups in the hash.
		foreach my $group (keys %groupsused) {
			 
			my $type	= $groupsused{$group};
			my $class	= $ref_options->{'recursive'}{$type}{$group}{'class'};
			
			# Look for objects within groups, based on a group's class.
			if ($class eq 'group_with_exception') {	# Groups with exclusions.
				foreach my $field ('base', 'exception') {
					my $exc_obj = (split /:/, $ref_options->{'recursive'}{$type}{$group}{$field})[-1];
					$objused{$type}{$exc_obj}++;
					if (exists $ref_options->{'recursive'}{$type}{$exc_obj}) {	# Object must exist in the db.
						$groupsused{$exc_obj} = $type if ! exists $groupschecked{$exc_obj} && $ref_options->{'recursive'}{$type}{$exc_obj}{'class'} =~ /_group|group_with_exception/;	# Object is a group, add it to the $groupsused hash.
					}
				}			
			} else {	# Other groups.
				foreach my $obj (keys %{$ref_options->{'recursive'}{$type}{$group}{'reference'}}) {
					$objused{$type}{$obj}++;	# Write the number of occurrences of an object.
					if (exists $ref_options->{'recursive'}{$type}{$obj}) {	# Object must exist in the db.
						$groupsused{$obj} = $type if ! exists $groupschecked{$obj} && $ref_options->{'recursive'}{$type}{$obj}{'class'} =~ /_group|group_with_exception/;	# Object is a group, add it to the $groupsused hash.
					}
				}
			}
			
			# Add current group to the hash with checked groups.
			$groupschecked{$group}++;
			
			# Delete processed group from the %groupsused hash.
			delete $groupsused{$group};	# Delete already processed groups.
			
		}
	}
	
	return \%objused;

}


#
# cpdb::compare($name, $ref_objentry, $ref_objdb)
#
# Compares an object with a database and returns a list of objects with the same parameters.
# NOTE: currently only node, networks, ranges and services are fully compared. For other objects, only names are checked.
# 
# Mandatory parameters:
#
#   - $name				Name of an object to be compared.
#   - $ref_objentry		Reference to a hash containing definition of an object to be compared.
#   - $ref_objdb		Reference to an object database to be compared with.
#
# Returns an hash reference:
#
#   $VAR = {
#     'match'	=> ['object1', 'object2'],	# Appears when matching objects are found.
#     'overlap'	=> 'object3'				# Appears when an object doesn't exactly match with others, but its name overlaps.
#   };
# 

sub compare {

	my($name, $ref_objentry, $ref_objdb) = @_;		# Capture subroutine parameters.
	
	my %compare = ();	# Hash containing information about matched objects and overlaps.
	my $type;			# Rule/object type ('network_objects', 'services', 'rule', 'rule_adtr')
	my $class;			# Object class.
	
	my %conditions = (		# Parameters to checked for each class of object.
		'host_plain'		=> ['ipaddr', 'add_adtr_rule', 'netobj_adtr_method', 'valid_ipaddr', 'the_firewalling_obj'],
		'network'			=> ['ipaddr', 'netmask', 'add_adtr_rule', 'netobj_adtr_method', 'valid_ipaddr', 'the_firewalling_obj'],
		'address_range'		=> ['ipaddr_first', 'ipaddr_last', 'add_adtr_rule', 'netobj_adtr_method', 'valid_ipaddr', 'the_firewalling_obj'],
		'tcp_service'		=> ['port', 'timeout', 'proto_type'],
		'udp_service'		=> ['port', 'timeout', 'proto_type'],
		'icmp_service'		=> ['icmp_type'],
		'dcerpc_service'	=> ['uuid'],
		'other_service'		=> ['protocol']
	);
	
	# Get the type and class of an entry. If not possible, return empty hash reference.
	if (exists $ref_objentry->{'class'} && exists $types{$ref_objentry->{'class'}}) {
		$type	= $types{$ref_objentry->{'class'}};
		$class	= $ref_objentry->{'class'};
	} else {
		return \%compare;
	}
	
	# Object being verified matches with a class found in the %conditions. Check all criteria.
	if (exists $conditions{$class}) {
	
		# Search for matching object and write them in the @match array.
		my @match = grep {
			my $return = 1;		# by default returned value is true.
			if ($ref_objdb->{$type}{$_}{'class'} eq $class) {		# Class matches - compare fields from the %conditions hash.
				foreach my $field (@{$conditions{$class}}) {
					if (exists $ref_objentry->{$field} && exists $ref_objdb->{$type}{$_}{$field}) {		# Objects have the same params.
						# Values must be the same. Otherwise set the $return to 0 (false).
						$return = 0 if $ref_objentry->{$field} ne $ref_objdb->{$type}{$_}{$field};
					} elsif (! exists $ref_objentry->{$field} != ! exists $ref_objdb->{$type}{$_}{$field}) {	# Objects don't have the same params.
						# Objects that don't have the same parameters are not matched. Always set the $return to 0 (false).
						$return = 0;
					}
				}
			} else { $return = 0; }		# If class doesn't match - return 'false'.
			$return;	# All criteria must be met to return a 'true' value.
		} keys %{$ref_objdb->{$type}};
		
		
		if (@match) {	# Found one or more objects that match all criteria.
			$compare{'match'} = \@match;
		} else {		# Didn't find an object that matches all criteria. Only check if names don't overlap (case-insensitive). 
			my($match) = grep { lc($name) eq lc($_) } keys %{$ref_objdb->{$type}};
			$compare{'overlap'} = $match if $match;
		}
		
	# Object doesn't match with any class from %conditions. Check only by name (case-insensitive) to avoid name overlapping.
	} else {
		my($match) = grep { lc($name) eq lc($_) } keys %{$ref_objdb->{$type}};
		$compare{'overlap'} = $match if $match;
	}
	
	return \%compare;

}


#
# cpdb::xlsx($row, $wsheet, $wbook, $id, $ref_entry, $ref_options)
#
# Prints information about particular objects or rules as an excel rows.
# Requires creating an excel workbook and worksheet using the Excel::Writer::XLSX module.
# Output is presented in the following columns:
#
#   - objects:		'Table / Object Type', 'Object Name', 'Object Class', 'Properties (IPs, ports, etc.)', 'Color', 'Comment',
#   - FW rules:		'Rule Disabled (true/false)', 'Rule Number', 'Rule Name', 'Source', 'Destination', 'VPN', 'Service', 'Action', 'Track', 'Install On', 'Comment',
#   - NAT rules:	'Rule Disabled (true/false)', 'Rule Number', 'Source', 'Destination', 'Service', 'Source Translated', 'Destination T.', 'Service T.', 'Install On', 'Comment'
#
# Mandatory parameters:
#
#   - $row			Row number used in excel (0 means row 1).
#   - $wsheet		Worksheet where the output should be placed.
#   - $wbook		Workbook where the output should be placed.
#   - $id			Object name or rule number to be printed.
#   - $ref_entry	Reference to a specific object or rule.
#
# Optional parameters:
#
#   $ref_options = {
#     'replace'		=> 'omit_old'/'show_both',   # 'omit_old' - replaced objects are omitted in the output; 
#                                                # 'show_both' - both old and new objects (in red) are printed.
#     'cmp'			=> {		# Reference to a compare hash containing objects and their replacements.
#       'object_type1'	=> {
#         'object1'	=> 'replacing_object',
#         'object2'	=> 'replacing_object'
#       },
#       'object_type2'	=> {
#         'object3'	=> 'replacing_object',
#         'object4'	=> 'replacing_object'
#       } 
#     },
#     'start_column'	=> number,	# Number of a column where the printing should be started. 0 by default.
#     'cell_format'		=> {...},	# Standard cell formatting.
#     'disabled_format'	=> {...},	# Disabled cell formatting.
#     'section_format'	=> {...},	# Section Title formatting.
#     'default_text'	=> {...},	# Default text.
#     'replace_text'	=> {...}	# Replacement object text.
#   }
#
# Default excel formatting:
#
#   - Standard cell:	text wrap, border, align (top),
#   - Disabled cell:	text wrap, border, align (top), background color (silver),
#   - Section Title:	bold, font color (white), border, background color (gray),
#   - Default text:		(none),
#   - Replacement text:	font color (red).
#
# Returns:
#
#   0 - row not printed (applicable for objects, when they have a replacement in the 'cmp' hash),
#   1 - row printed
#

sub xlsx {

	my($row, $wsheet, $wbook, $id, $ref_entry, $ref_options) = @_;		# Capture subroutine parameters.
	
	my $startcol	= (exists $ref_options->{'start_column'})	? $ref_options->{'start_column'}	: 0;	# Starting column number, default is 0.
	my $replace		= (exists $ref_options->{'replace'})		? $ref_options->{'replace'}			: 0;	# Replacement option ('omit_old'/'show_both'), default is 0.
	my $ref_cmp		= (exists $ref_options->{'cmp'})			? $ref_options->{'cmp'}				: {};	# Reference to a compare hash, by default it is an empty hash ref.
	
	my @paramscell;		# Content of a cell with multiple parameters.
	my $type;			# Rule/object type ('network_objects', 'services', 'rule', 'rule_adtr')

	# Get the type of an entry. If not possible, return 0.
	if (exists $ref_entry->{'class'} && exists $types{$ref_entry->{'class'}}) {
		$type = $types{$ref_entry->{'class'}};
	} else {
		return 0;
	}
	
	
	# Default excel formatting.
	my %formatting = (
		'cell_format'		=> {	# Standard cell formatting.
			'text_wrap'	=> 1,
			'border'	=> 1,
			'align'		=> 'top'
		},
		'disabled_format'	=> {	# Disabled cell formatting.
			'text_wrap'	=> 1,
			'border'	=> 1,
			'align'		=> 'top',
			'bg_color'	=> 'silver'
		},
		'section_format'	=> {	# Section Title formatting.
			'bold'		=> 1,
			'border'	=> 1,
			'color'		=> 'white',
			'bg_color'	=> 'gray'
		},
		'default_text'		=> {},	# Default text.
		'replace_text'		=> {	# Replacement object text.
			'color' => 'red'
		}
	
	);
	
	# Change the default excel formatting based on user options.
	# Only explicitly defined options are changed. Other maintain their default values.
	foreach my $option (keys %$ref_options) {
		if (exists $formatting{$option}) {
			foreach (keys %{$ref_options->{$option}}) {
				$formatting{$option}{$_} = $ref_options->{$option}{$_};
			}
		}
	}
	
	# Excel formatting variables definitions.
	my $fcell		= $wbook->add_format(%{$formatting{'cell_format'}});		# Define standard cell formatting.
	my $fdiscell	= $wbook->add_format(%{$formatting{'disabled_format'}});	# Define disabled cell formatting.
	my $fsection	= $wbook->add_format(%{$formatting{'section_format'}});	# Define formatting for a Section Title.
	my $fdefault	= $wbook->add_format(%{$formatting{'default_text'}});		# Define default text formatting.
	my $freplace	= $wbook->add_format(%{$formatting{'replace_text'}});		# Define text formatting for object replacements.
	
	
	# Handle objects.
	if ($type =~ /network_objects|services/) {
	
		if (exists $ref_cmp->{$type}{$id} && $replace =~ /omit_old|show_both/) {		# Replacements options set, replacement for an object exists - do nothing and return 0.
		
			return 0;
		
		} else 	{		# No replacement for an object - proceed with creating a new row in excel.
		
			# Initial multi-param cell formatting and content.
			$paramscell[0] = $fdefault;
			$paramscell[1] = '';
		
			# Parameters in a multi-param cell.
			my @params = (
				'ipaddr',
				'netmask',
				'ipaddr_first',
				'ipaddr_last',
				'protocol',
				'port',
				'timeout',
				'icmp_type',
				'uuid',
				'netobj_adtr_method',
				'valid_ipaddr',
				'the_firewalling_obj',
				'reference'
			);
		
			# Calculate 'Properties' (multi-param) cell content.
			foreach my $param (@params) {
				if (exists $ref_entry->{$param}) {
					if ($param eq 'reference') {	# Handle 'reference' section.
						$paramscell[-1] .= "$param=\{\n";	# Beginning of the 'reference' section.
						foreach my $member (keys %{$ref_entry->{$param}}) {
							if ($replace eq 'omit_old') {			# Omit old references and show only replacing objects.
								$paramscell[-1] .= (exists $ref_cmp->{$type}{$member}) ? "    $ref_cmp->{$type}{$member}\n" : "    $member\n";
							} elsif ($replace eq 'show_both') {	# Show old and replacing objects. Replacements are in '()'.
								$paramscell[-1] .= "    $member";
								if (exists $ref_cmp->{$type}{$member}) {
									push @paramscell, $freplace;
									push @paramscell, " \($ref_cmp->{$type}{$member}\)";
									push @paramscell, $fdefault;
									push @paramscell, '';
								}
								$paramscell[-1] .= "\n";
							} else {	# No replacement option is set. Just print members.
								$paramscell[-1] .= "    $member\n";
							}
						}
						$paramscell[-1] .= "\}\n";		# End of the 'reference' section.
					} else {	# Handle all other parameters.
						$paramscell[-1] .= "$param=$ref_entry->{$param}\n";
					}
				}
	
			}
	
			# Write values into cells.
			$wsheet->write_rich_string($row, $startcol, $type, $fcell);
			$wsheet->write_rich_string($row, $startcol + 1, $id, $fcell);
			$wsheet->write_rich_string($row, $startcol + 2, $ref_entry->{'class'}, $fcell);
			$wsheet->write_rich_string($row, $startcol + 3, @paramscell, $fcell);
			$wsheet->write_rich_string($row, $startcol + 4, $ref_entry->{'color'}, $fcell);
			$wsheet->write_rich_string($row, $startcol + 5, $ref_entry->{'comments'}, $fcell);
	
		}
	}
	
	
	# Handle FW and NAT rules.
	if ($type =~ /rule/) {
	
		my $disabled = $ref_entry->{'disabled'};	# Rule is disabled? (true/false).
		
		# Cell format (standard/disabled), based on the $disabled value.
		my %cellformat = (
			'true'	=> $fdiscell,
			'false'	=> $fcell,
		);
		
		if ($ref_entry->{'class'} =~ /header_rule/) {		# Handle Section Titles.
		
			my %size = ('rule' => 10, 'rule_adtr' => 9);		# Size of the section, depending on the rule type.
			$wsheet->merge_range($row, $startcol, $row, $startcol + $size{$type}, $ref_entry->{'header_text'}, $fsection );
			
		} else {		# Handle standard rules.
		
			my $column = 2;		# Set column counter.
			
			my @fields = (		# Fields with multiple params.
				'src',
				'dst',
				'through',
				'services',
				'action',
				'track',
				'src_adtr',
				'dst_adtr',
				'services_adtr',
				'src_adtr_translated',
				'dst_adtr_translated',
				'services_adtr_translated',
				'install'
			);
		
			# Write values into the 'Disabled' and 'No.' columns.
			$wsheet->write_rich_string($row, $startcol, $disabled, $cellformat{$disabled});
			$wsheet->write_rich_string($row, $startcol + 1, $id, $cellformat{$disabled});
			
			# Firewall rules specific entry ('Name').
			if ($type eq 'rule') {
				$wsheet->write_rich_string($row, $startcol + $column, $ref_entry->{'name'}, $cellformat{$disabled});
				$column++;
			}
			
			# Handle fields with multiple params.
			foreach my $field (@fields) {
				if (exists $ref_entry->{$field}) {
					
					# Indicates if a cell is negated or not. If negated, all objects will have '(X)' sign at the beginning.
					my $negate = (exists $ref_entry->{$field}{'_op'} && $ref_entry->{$field}{'_op'} eq '"not in"') ? '(X) ' : '';
					
					# Initial multi-param cell formatting and content.
					$paramscell[0] = $fdefault;
					$paramscell[1] = (exists $ref_entry->{$field}{'_translate'}) ? "$ref_entry->{$field}{'_translate'}:\n" : '';		# Write special entries (e.g. NAT methods) if they exist.
					
					# Calculate content of particular multi-param field.
					foreach my $obj (keys %{$ref_entry->{$field}}) {
						next if $obj =~ /^_/;		# Omit all special parameters (beginning with '_').
						my $objtype = $ref_entry->{$field}{$obj};
						if ($replace eq 'omit_old') {		# Omit old references and show only replacing objects.
							$paramscell[-1] .= (exists $ref_cmp->{$objtype}{$obj}) ? "$negate$ref_cmp->{$objtype}{$obj}\n" : "$negate$obj\n";							
						} elsif ($replace eq 'show_both') {	# Show old and replacing objects. Replacements are in '()'.
							$paramscell[-1] .= "$negate$obj";
							if (exists $ref_cmp->{$objtype}{$obj}) {
								push @paramscell, $freplace;
								push @paramscell, " \($ref_cmp->{$objtype}{$obj}\)";
								push @paramscell, $fdefault;
								push @paramscell, '';						
							}
							$paramscell[-1] .= "\n";
						} else {	# No replacement option is set. Just print objects.
							$paramscell[-1] .= "$negate$obj\n";
						}
					}
					
					# Write values into a multi-param cell and move to the next column.
					$wsheet->write_rich_string($row, $startcol + $column, @paramscell, $cellformat{$disabled});
					@paramscell = ();
					$column++;
					
				}
			}
			
			# Write values into the 'Comment' column.
			$wsheet->write_rich_string($row, $startcol + $column, $ref_entry->{'comments'}, $cellformat{$disabled});
		
		}
		
	}
	
	# Return print status.
	return 1;

}


#
# cpdb::auto2maual($name, $ref_obj, $ref_options)
#
# Converts automatic NAT configuration to manual NAT rules.
# Creates objects representing both the original object ('<name>_orig') and NAT IP addresses ('<name>_static/hide').
#
# Mandatory parameters:
#
#   - $name			Name of the object that contains automatic NAT configuration.
#   - $ref_obj		Reference to the object configuration.
#
# Optional parameters:
#
#   $ref_options = {
#     'hidding_gw'	=> 'gw_name',   # Name of a gateway to be used in manual NAT rules when the 'Hide behind Gateway' option is set.
#   }
#
# Returns 'undef' if an object doesn't have automatic NAT settings, otherwise returns a hash reference:
#
#   $VAR = {
#     'objects'	=> {
#       'object_orig'	=> {...},	# Reference to a hash with objects configuration.
#       'object_hide'	=> {...}	# Reference to a hash with objects configuration.
#     },
#     'rules'	=> [
#       {...},	# Hash reference to the NAT rule config.
#       {...}	# Hash reference to the NAT rule config.
#     ]
#   };
#

sub auto2manual {

	my($name, $ref_obj, $ref_options) = @_;		# Capture subroutine parameters.
	
	# Get the 'hidding_gw' option value (default is 0).
	my $hidegw = (exists $ref_options->{'hidding_gw'}) ? $ref_options->{'hidding_gw'} : 0;
	
	# Returned value - a hash with created NAT rules and objects.
	my $ref_output;
	
	# Create NAT rules only for objects that have automatic configuration.
	if ($ref_obj->{'add_adtr_rule'} eq 'true') {
	
		# Set common object variables.
		my $name_orig	= $name . "_orig";	# Name of a copy of an object with automatic NAT.
		my $name_nat	= ($ref_obj->{'netobj_adtr_method'} eq 'adtr_hide') ? $name . "_hide" : $name . "_static";	# Name of a NAT object.
		my($inst_type, $inst_obj) = split /:/, $ref_obj->{'the_firewalling_obj'};		# Install-on object (type and name).
		
		$inst_obj =~ s/^All$/Any/;	# Replace 'All' to 'Any' in the install-on object name.
	
		# Create a copy of the original object, but without automatic NAT settings.
		$ref_output->{'objects'}{$name_orig}{'add_adtr_rule'} = 'false';
		foreach ('class', 'ipaddr','netmask', 'ipaddr_first', 'ipaddr_last', 'color', 'comments') {
			$ref_output->{'objects'}{$name_orig}{$_} = $ref_obj->{$_} if exists $ref_obj->{$_};
		}
		
		# Create representation of the NAT object (if 'Hide behind Gateway' in GUI and 'hidding_gw' options are not set).
		if (not($hidegw && $ref_obj->{'valid_ipaddr'} eq '0.0.0.0')) {
			$ref_output->{'objects'}{$name_nat}{'class'}			= 'host_plain';
			$ref_output->{'objects'}{$name_nat}{'add_adtr_rule'}	= 'false';
			$ref_output->{'objects'}{$name_nat}{'ipaddr'}			= $ref_obj->{'valid_ipaddr'};
			$ref_output->{'objects'}{$name_nat}{'color'}			= $ref_obj->{'color'};
			$ref_output->{'objects'}{$name_nat}{'comments'}		= $ref_obj->{'comments'};
		}
	
		# Common NAT rules parameters.
		foreach (0..1) {
			$ref_output->{'rules'}[$_]{'name'}		= '';
			$ref_output->{'rules'}[$_]{'class'}	= 'address_translation_rule';
			$ref_output->{'rules'}[$_]{'disabled'}	= 'false';
			$ref_output->{'rules'}[$_]{'comments'}	= "(Automatic rule) $ref_obj->{'comments'}";
			$ref_output->{'rules'}[$_]{'install'}	= {$inst_obj => $inst_type};
			$ref_output->{'rules'}[$_]{'services_adtr'}	= {'Any' => 'globals'};
			$ref_output->{'rules'}[$_]{'services_adtr_translated'}	= {'services_adtr_translated' => 'service_translate', 'Any' => 'globals'};
		}
	
		# NAT fields, different for each type of automatic NAT.
		if ($ref_obj->{'netobj_adtr_method'} eq 'adtr_hide') {
		
			# Choose the hidding object (gateway from the 'hidding_gw' option or the created object).
			my $hide = ($hidegw && $ref_obj->{'valid_ipaddr'} eq '0.0.0.0') ? $hidegw : $name_nat;
			
			# First rule (all objects except host).
			if ($ref_obj->{'class'} ne 'host_plain') {
				$ref_output->{'rules'}[0]{'src_adtr'} 		= {$name_orig => 'network_objects'};
				$ref_output->{'rules'}[0]{'dst_adtr'} 		= {$name_orig => 'network_objects'};
				$ref_output->{'rules'}[0]{'src_adtr_translated'} 		= {'src_adtr_translated' => 'translate_static', 'Any' => 'globals'};
				$ref_output->{'rules'}[0]{'dst_adtr_translated'} 		= {'dst_adtr_translated' => 'translate_static', 'Any' => 'globals'};
			}
			
			# Second rule.
			$ref_output->{'rules'}[1]{'src_adtr'} 		= {$name_orig => 'network_objects'};
			$ref_output->{'rules'}[1]{'dst_adtr'} 		= {'Any' => 'globals'};
			$ref_output->{'rules'}[1]{'src_adtr_translated'} 		= {'src_adtr_translated' => 'translate_hide', $hide => 'network_objects'};
			$ref_output->{'rules'}[1]{'dst_adtr_translated'} 		= {'dst_adtr_translated' => 'translate_static', 'Any' => 'globals'};
		
		} else {
			
			# First rule.
			$ref_output->{'rules'}[0]{'src_adtr'} 		= {$name_orig => 'network_objects'};
			$ref_output->{'rules'}[0]{'dst_adtr'} 		= {'Any' => 'globals'};
			$ref_output->{'rules'}[0]{'src_adtr_translated'} 		= {'src_adtr_translated' => 'translate_static', $name_nat => 'network_objects'};
			$ref_output->{'rules'}[0]{'dst_adtr_translated'} 		= {'dst_adtr_translated' => 'translate_static', 'Any' => 'globals'};

			# Second rule.
			$ref_output->{'rules'}[1]{'src_adtr'} 		= {'Any' => 'globals'};
			$ref_output->{'rules'}[1]{'dst_adtr'} 		= {$name_nat => 'network_objects'};
			$ref_output->{'rules'}[1]{'src_adtr_translated'} 		= {'src_adtr_translated' => 'translate_static', 'Any' => 'globals'};
			$ref_output->{'rules'}[1]{'dst_adtr_translated'} 		= {'dst_adtr_translated' => 'translate_static', $name_orig => 'network_objects'};		
		}
	
	}
	
	# Return a value: undef if input object doesn't have automatic NAT, hash reference if an object has auto NAT.
	return $ref_output;

}


# Force package to return 'true'.
1;
